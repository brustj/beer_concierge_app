import { UPDATE_BEERS } from 'actions';

export const beers = (state = [], action) => {
  switch (action.type) {
    case UPDATE_BEERS:
      return action.beers;
    default:
      return state;
  }
};
