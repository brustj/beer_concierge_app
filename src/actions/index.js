export const UPDATE_BEERS = 'UPDATE_BEERS';

export const updateBeers = beers => {
  return {
    type: UPDATE_BEERS,
    beers
  }
};