import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as Utils from 'utils';

import BeerCard from 'components/BeerCard';

import { updateBeers } from 'actions';

import 'App.scss';

class App extends Component {
	componentDidMount() {
		const { dispatch } = this.props;

		window.addEventListener('message', event => {
			if (event.data.action === 'LEX_DATA_RECIEVED' && event.data.data.dialogState === 'Fulfilled') {
				const type = event.data.data.slots.BeerType.toLowerCase();
				const traits = event.data.data.slots.BeerTraits.toLowerCase();
				// const locale = event.data.data.slots.BeerLocale;

				new Promise((resolve, reject) => {
					Utils.getBeers(`https://rmuevdnnu8.execute-api.us-east-1.amazonaws.com/beta/?q=${type}&withBreweries=Y&type=beer&p=`, [], 1, resolve, reject);
				})
					.then(response => {
						const filteredResponse = response.filter(beer => {
							return (
								beer.region === 'Washington' ||
								beer.region === 'California' || 
								beer.region === 'Oregon' ||
								beer.region === 'Idaho'
							) && beer.desc.indexOf(traits) !== -1;
						});

						dispatch( updateBeers(filteredResponse) );
					});
			}
		}, false);

		// window.postMessage({ action: 'LEX_DATA_RECIEVED', data: { dialogState: 'Fulfilled', slots: { BeerType: 'ipa', 'BeerTraits': 'hoppy', 'BeerLocale': 'yes' } } }, '*');
	}
  render() {
		const { beers } = this.props;

    return (
      <div className='app' data-show={beers.length > 0 ? true : false}>
				{ beers.map((beer, index) =>
					<BeerCard beer={beer} key={index} />
				)}
			</div>
    );
  }
}

const mapStateToProps = state => ({
  beers: state.beers
});

export default connect(mapStateToProps)(App);