import axios from 'axios'

export const getBeers = (url, beers, pageNumber, resolve, reject) => {
  axios.get(`${url}${pageNumber}`)
    .then(response => {
      console.log( response );
			response.data.data.forEach(beer => {
				beers.push({
					name: beer.name,
					brewery: beer.breweries ? beer.breweries[0].name : 'n/a',
					desc: beer.description ? beer.description : (beer.breweries ? beer.breweries[0].description : 'n/a'),
					abv: beer.abv ? `${beer.abv}%` : 'n/a',
          image: beer.labels ? beer.labels.contentAwareLarge : (beer.breweries && beer.breweries[0].images ? beer.breweries[0].images.squareLarge : ''),
          region: beer.breweries && beer.breweries[0].locations ? beer.breweries[0].locations[0].region : '',
          ibu: beer.ibu ? beer.ibu : 'n/a'
				});
      });
      
      console.log( `fetching beer results ${pageNumber} of ${response.data.numberOfPages}` );

      if (pageNumber < response.data.numberOfPages) {
        getBeers(url, beers, (pageNumber + 1), resolve, reject);
      } else {
        resolve(beers);
      }
    })
    .catch(err => {
      reject( `Error: ${err}` );
    })
}