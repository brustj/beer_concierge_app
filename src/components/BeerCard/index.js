import React from 'react';

import './index.scss';

const BeerCard = ({ beer }) => {
  return (
    <div className='beerCard'>
      <div className='inner'>
        <img src={beer.image} width='100%' height='auto' alt={beer.name} />

        <div className='name'>{beer.name}</div>

        <div className='label'>Brewery</div>
        <p>{beer.brewery}</p>

        <div className='splitContainer'>
          <div className='splitSection' style={{'width': '50%'}}>
            <div className='label'>Region</div>
            <p>{beer.region}</p>
          </div>

          <div className='splitSection'>
            <div className='label'>ABV</div>
            <p>{beer.abv}</p>
          </div>

          <div className='splitSection'>
            <div className='label'>IBU</div>
            <p>{beer.ibu}</p>
          </div>
        </div>

        <div className='label'>Description</div>
        <p>{`${beer.desc.substr(0, 120)}...`}</p>
      </div>
    </div>
  )
}

export default BeerCard;
